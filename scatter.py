import matplotlib.pyplot as plt
import json
import numpy as np

likes = []
alts = []
colors = []

with open("alternativeto/apps_uniq.jl") as f:
    for line in f:
        app = json.loads(line.strip())
        if app['app_alt_cnt'] < 50:
            likes.append(app['app_votes'])
            alts.append(app['app_alt_cnt'])
            r = app['app_votes'] / (app['app_alt_cnt'] or 0.0001)
            colors.append(r*1000)

likes_logged = np.log(likes)

plt.scatter(alts, likes_logged, c=colors)
plt.show()
