#!/bin/bash

cat alternativeto/apps_uniq.jl | python -c "import sys; lines=sys.stdin.readlines(); import json; lines_dicts=[json.loads(line.strip()) for line in lines]; sorted_lines=sorted(lines_dicts,key=lambda app: float(app['app_votes'])/float(app['app_alt_cnt']) if app['app_alt_cnt']>0 else 0.0001, reverse=True); from pprint import pprint; pprint(sorted_lines[:50])"