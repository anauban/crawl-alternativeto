import matplotlib.pyplot as plt
import json
import numpy as np

likes = []
alts = []
colors = []
likes_first_digit = []

with open("alternativeto/apps_uniq.jl") as f:
    for line in f:
        app = json.loads(line.strip())
        likes.append(app['app_votes'])
        likes_first_digit.append(int(str(app['app_votes'])[0]))
        alts.append(app['app_alt_cnt'])


#plt.xlim([0,100])
binwidth = 10
#plt.hist(sorted(likes),  bins=range(min(likes), max(likes) + binwidth, binwidth))

# plot histogram of first digit distribution
# note that this data is not normally distributed and has unproportionately high number of zeroes
plt.hist(likes_first_digit)
plt.show()
