# -*- coding: utf-8 -*-
import scrapy
import re
from alternativeto.items import AlternativetoItem
from scrapy.http import Request

class AppsSpider(scrapy.Spider):
    name = "apps"
    allowed_domains = ["alternativeto.net"]
    start_urls = (
        'http://alternativeto.net/?sort=likes',
    )

    MAX_PAGES = 1500

    def parse(self, response):
        # build requests for all pages
        # Note: empirically found 1500 to be > actual number of pages, so we don't miss any.
        # This will have to be manually updated
        
        page_link = "http://alternativeto.net/?sort=likes&page=%s"

        for page in range(1,self.MAX_PAGES):
            yield Request(page_link%page, self.parse_page)

    def parse_page(self, response):
        items = response.xpath("//li[@class='item-wrapper']")
        for item in items:
            app = AlternativetoItem()
            app['app_title'] = item.xpath("div[@class='item-header']/h3/a/text()").extract()[0]
            app['app_link'] = "http://www.alternativeto.net" + item.xpath("div[@class='item-header']/h3/a/@href").extract()[0]
            app['app_votes'] = int(re.sub(",","",item.xpath(".//div[@class='icon-num-box']/div/text()").extract()[0]))
            app_alt_cnt_node = item.xpath("div[@class='item-header']/div[@class='subheader']/text()[normalize-space()]").extract()[0]
            m = re.match(".*?([0-9\.,]+) Alternatives.*", app_alt_cnt_node, re.DOTALL)
            if m:
                app['app_alt_cnt'] = int(re.sub(",","",m.group(1)))

            if 'app_alt_cnt' in app and app['app_alt_cnt']:

                yield app

            else:
                yield Request(url = app['app_link'], callback = self.parse_app, meta={'app':app})

    def parse_app(self, response):
        app = response.meta['app']

        alts = response.xpath("//li[@id='tabAlternatives']//span/text()").extract()[0]

        app['app_alt_cnt'] = int(re.sub(",","",alts))

        return app
