# -*- coding: utf-8 -*-

# Scrapy settings for alternativeto project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'alternativeto'

SPIDER_MODULES = ['alternativeto.spiders']
NEWSPIDER_MODULE = 'alternativeto.spiders'

# If I get blocked don't cache the page
HTTPCACHE_IGNORE_HTTP_CODES = ['500']

USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'alternativeto (+http://www.yourdomain.com)'
