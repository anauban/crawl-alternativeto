# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class AlternativetoItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    app_title = scrapy.Field()
    app_link = scrapy.Field()
    app_votes = scrapy.Field()
    app_alt_cnt = scrapy.Field()
